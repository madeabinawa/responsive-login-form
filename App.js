/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */

import React from 'react';
import { View, Text, Image, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

const App = () => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Image style={styles.img} source={{
        uri: 'https://codelatte.org/wp-content/uploads/2018/07/fixcil.png',
      }} />
      <Text style={styles.text}>LOGIN HERE</Text>
      <TextInput
        style={styles.textInput}
        placeholder="Username" />
      <TextInput
        style={styles.textInput}
        secureTextEntry={true}
        placeholder="Password" />
      <TouchableOpacity style={styles.btn}>
        <Text style={styles.btnText}>LOGIN</Text>
      </TouchableOpacity>
    </View >
  );
};

const styles = StyleSheet.create({
  img: {
    width: responsiveWidth(30.5),
    height: responsiveHeight(17),


  },
  textInput: {
    borderColor: 'lightblue',
    borderStyle: 'solid',
    borderWidth: 2,
    borderRadius: 10,
    height: responsiveHeight(7),
    width: responsiveWidth(80),
    padding: responsiveHeight(2),
    margin: responsiveHeight(1),
  },
  text: {
    margin: responsiveHeight(2),
    color: 'dodgerblue',
    fontSize: responsiveFontSize(2.2),
    fontWeight: '700',
  },
  btn: {
    height: responsiveHeight(7),
    width: responsiveWidth(80),
    padding: responsiveHeight(2),
    margin: responsiveHeight(1),
    backgroundColor: 'dodgerblue',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',

  },
  btnText: {
    color: 'white',
    fontSize: responsiveFontSize(2.2),
    fontWeight: '700',

  },
});

export default App;
